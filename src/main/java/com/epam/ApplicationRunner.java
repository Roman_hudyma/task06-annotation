package com.epam;

import com.epam.view.AnnotationView;

public class ApplicationRunner {
    public static void main(String[] args) {
        new AnnotationView().run(new AnnotationView().getPerson());
    }
}
