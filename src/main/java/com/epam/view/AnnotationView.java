package com.epam.view;

import com.epam.controller.PersonController;
import com.epam.controller.UnknownClassController;
import com.epam.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class AnnotationView {
    private static final Logger log = LogManager.getLogger();
    Person person =new Person("Andrey",18,"Mercedes-Benz",true);
    PersonController controller =new PersonController(person);
   Class unknownClass = person.getClass();
    String houses;



    public Person getPerson() {
        return person;
    }

    public void run(Person person)
    {
        Field[] declaredFields = unknownClass.getDeclaredFields();
        try {
            for (Field declaredField : declaredFields) {
                log.info(declaredField.getType() + " " + declaredField.getName());
            }
        }

        catch (Exception e)
        {
            log.error(e);
        }
        new UnknownClassController().getUnknownClassInfo(person);
        {
        try {
            houses = controller.getHouses("Roman","Apartment","Cottage","Bungalow");
        } catch (NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
       log.info(houses);


        try {
            controller.setUnknownTypeParameter("Roman");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

