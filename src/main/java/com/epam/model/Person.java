package com.epam.model;

public class Person {
    @MyAnnotation(name = "Roman", age = 20, car = "Nissan X-trail", house = true)
    private String name;
    private int age;
    private String car;
    private boolean house;

    public Person(String name, int age, String car, boolean house) {
        this.name = name;
        this.age = age;
        this.car = car;
        this.house = house;
    }
    public String getHouses(String name,String...houses){
        String ownerHouses = null;
        for(String house:houses)
        {
            ownerHouses+=house;
        }
        return name+" houses:"+ownerHouses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public boolean isHouse() {
        return house;
    }

    public void setHouse(boolean house) {
        this.house = house;
    }
}
