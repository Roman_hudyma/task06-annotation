package com.epam.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface MyAnnotation {
    String name()default "Noname";
    int age()default 18;
    String car()default "BMW-X1";
    boolean house()default true;
}
