package com.epam.controller;

import com.epam.model.MyAnnotation;
import com.epam.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PersonController {
    private static final Logger log = LogManager.getLogger();
    private Person person;
    private Class aClass;


    public PersonController(Person person) {
        this.person = person;
        this.aClass = aClass;
    }




    public void getFields() {
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (declaredField.isAnnotationPresent(MyAnnotation.class)) {
                declaredField.setAccessible(true);
                MyAnnotation annotation = declaredField.getAnnotation(MyAnnotation.class);
                log.info("Information:" + annotation.name() + "-" + annotation.age() + "-" + annotation.car()+"-"+annotation.house());
            }
        }

    }
    public String getHouses(String name, String... houses) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method getHouses = aClass.getDeclaredMethod("getHouses", String.class, int[].class);
        return (String) getHouses.invoke(person, name, houses);
    }
    public void setUnknownTypeParameter(String unknown) throws NoSuchFieldException, IllegalAccessException {
        Field partNumber = aClass.getField("car");
        partNumber.setAccessible(true);
        partNumber.set(person, unknown);
    }
}
